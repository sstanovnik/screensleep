# ScreenSleep
A utility to force screen sleep on demand. 

ScreenSleep is a Windows application that, when run, puts all computer monitors to sleep in the same way as an idle timeout does. 


## Why?
Desktop computers generally don't have a hotkey to turn their screen off on demand. 

Sometimes you want to have something running in the background and just turn the display off immediately, so putting the whole computer to sleep isn't viable. 

Turning off your monitor with the physical button? Having more than one monitor makes this a bit tedious. Moreover, some monitors "disconnect" when powered off, so window layouts in multi-monitor setups can be compromised. 


## Requirements
 * Windows
 * .NET Framework 4.5

