﻿using System;
using System.Threading;

namespace ScreenSleep
{
    class Program
    {
        static void Main(string[] args)
        {
            Thread.Sleep(500);
            WinAPI.SendNotifyMessage(WinAPI.HWND_BROADCAST, (uint) WinAPI.WM.SYSCOMMAND, (UIntPtr) WinAPI.SysCommands.SC_MONITORPOWER, (IntPtr) 2);
        }
    }
}
